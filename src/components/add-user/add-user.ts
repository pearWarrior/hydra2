import Vue from 'vue';
import Component from 'vue-class-component';
import DatePicker from 'vuejs-datepicker';
import './add-user.scss';
import { ErrorBag } from "vee-validate";

function getEmptyUser(): IUser {
  const EMPTY_USER = {
    username: '',
    email: '',
    phone: '',
    birthday: new Date().toISOString(),
  };
  return Object.assign({}, EMPTY_USER);
}

@Component({
  template: require('./add-user.pug'),
  components: { DatePicker }
})
export class AddUser extends Vue {
  private user: IUser = getEmptyUser();
  private errors: ErrorBag;

  onSubmit(): void {
    this.$validator.validateAll().then((result) => {
      if (!result) {
        return;
      }

      this.$emit('add-user', this.user);
      this.user = getEmptyUser();
      this.$validator.reset();
    });
  }
}
