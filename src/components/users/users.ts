import Vue from 'vue';
import Component from 'vue-class-component';
import { orderBy } from 'lodash';
import { formatDate } from '../../util/date'

import './users.scss';

const ORDER = {
  ASC: 'asc',
  DESC: 'desc',
};

@Component({
  template: require('./users.pug'),
  props: {
    data: Array
  }
})
export class Users extends Vue {
  private data: IUser[];
  private isAsc: boolean = true;

  get users(): IUser[] {
    return orderBy(this.data, 'email', <any>this.isAsc ? ORDER.ASC : ORDER.DESC);
  }

  parseDate(date: string): string {
    return formatDate(date);
  }

  toggleSortMode(): void {
    this.isAsc = !this.isAsc;
  }
}
