import Vue from 'vue';
import VeeValidate from 'vee-validate';
import { Users } from './components/users/users';
import { AddUser } from './components/add-user/add-user';
// import './css/app.css';

Vue.use(VeeValidate);

const users = JSON.parse(require('./users.json'));


new Vue({
  el: '#app-main',
  components: {
    AddUser,
    Users,
  },
  methods: {
    onUserAdd: (user: IUser) => users.push(user)
  },
  data: () => {
    return {
      users
    }
  }
});
