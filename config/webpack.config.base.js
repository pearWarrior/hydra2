const helpers = require('./helpers');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const vueLoaderConfig = require('./vue-loader.conf');

let config = {
  entry: {
    'app': helpers.root('/src/app.ts')
  },
  output: {
    path: helpers.root('/dist'),
    filename: 'js/[name].[hash].js'
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.js', '.html', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      'muse-components': 'muse-ui/src'
    }
  },
  module: {
    rules: [
      {
        test: /.less$/,
        loader: 'css-loader!less-loader',
      },
      {
        test: /\.css$/,
        loader: ['style-loader', 'css-loader']
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        enforce: 'pre',
        loader: 'tslint-loader'
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.pug$/,
        loader: ['raw-loader', 'pug-html-loader'],
      },
      {
        test: /\.html$/,
        loader: 'raw-loader',
        exclude: ['./src/index.html']
      },
      {
        test: /\.json$/,
        loader: 'raw-loader'
      }
    ],
  },
  plugins: [
    new CopyWebpackPlugin([{
      from: 'src/assets',
      to: './assets'
    }, ]),

    new ExtractTextPlugin("[name].css")
  ]
};

module.exports = config;
