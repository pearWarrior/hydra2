'use strict'
const utils = require('./utils');

module.exports = {
  loaders: utils.cssLoaders({
    sourceMap: true,
    extract: false
  }),
  transformToRequire: {
    video: 'src',
    source: 'src',
    img: 'src',
    image: 'xlink:href'
  }
}
