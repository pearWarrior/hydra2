declare interface IUserBase {
  username: string;
  email: string;
  phone: string;
}

declare interface IUser extends IUserBase {
  birthday: string;
}
