FROM node:8.9-alpine as builder
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
EXPOSE 8080
CMD ["npm", "run", "dev"]
